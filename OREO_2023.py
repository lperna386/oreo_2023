# -*- coding: utf-8 -*-
"""
Created on Fri Aug 18 14:56:47 2023

File di analisi simulazioni
@author: Leo
"""
#%% Imports pacchetts (anche cose superflue probably)

import uproot
import numpy as np
import matplotlib.pyplot as plt
import copy
import matplotlib as mpl
from matplotlib.colors import ListedColormap
from scipy.optimize import curve_fit
import os



#%% Importo dati 


#dati in amorfo
data_path_amo = 'C:/Users/Leo/Desktop/UNI/OREO/Outputfiles/tbeamdata_amorphous.root'

data_amo = uproot.open(data_path_amo)['outData']

print(data_amo.keys())

#dati in assiale

data_path_axial = 'C:/Users/Leo/Desktop/UNI/OREO/Outputfiles/tbeamdata_axial.root'

data_axial = uproot.open(data_path_axial)['outData']

print(data_axial.keys())


#%% Check to understand: 
    
#provo a vedere se hanno senso le posizioni, per es in amorfo 
    
x1 = np.array(data_amo['Tracker_X_0'])

y1 = np.array(data_amo['Tracker_Y_0'])

x2 = np.array(data_amo['Tracker_X_1'])

y2 = np.array(data_amo['Tracker_Y_1'])


#%% Proviamo a fare isto di una delle posizioni togliendo i valori senza senso

h, bins = np.histogram(x1, bins = 100, range= [-5,5])

binc = bins[:-1]+(bins[1]-bins[0])/2 

fig, ax = plt.subplots()

fig.set_size_inches(15,15)
    
ax.plot(binc,h, ds='steps-mid',color = 'darkgreen')

ax.grid()

#ha senso, nella simulazione abbiamo messo forma rettangolare


#%% Deposito energetico nei cristalli in amorfo 

ph_a_amo = np.array(data_amo['CrystalA_EDep'])

cond_a_amo = ph_a_amo !=0 #tolgo gli eventi in cui i cristalli non vengono colpiti, cioe quando in ph_i = 0 (puo succedere che la part passa i tracker e non attraversa i crsitalli )


ph_b_amo = np.array(data_amo['CrystaB_EDep'])

cond_b_amo = ph_b_amo !=0

ph_c_amo = np.array(data_amo['CrystalC_EDep'])

cond_c_amo = ph_c_amo !=0
 
ph_amo = {'crisA': ph_a_amo, 'crisB': ph_b_amo, 'crisC': ph_c_amo}

cond_amo = {'crisA': cond_a_amo, 'crisB': cond_b_amo, 'crisC': cond_c_amo}

fig, ax = plt.subplots(3,1)

ax = ax.flatten()

fig.set_size_inches(15,15)
    
for i,key in enumerate(ph_amo.keys()) :
    
    
    h, bins = np.histogram(ph_amo[key][cond_amo[key]], bins = 100) #istogrammo solo gli eventi che hanno depositato qualcosa nel cristallo, e non rigorosamente zero 

    binc = bins[:-1]+(bins[1]-bins[0])/2
    
    
    ax[i].plot(binc,h, ds='steps-mid',color = 'darkgreen', label = f'{i+1}-th crystal- amorphous config')

    ax[i].grid()
    
    ax[i].set_yscale('log')
    
    ax[i].legend(fontsize = 18)
    
    


#%% Deposito energetico nei cristalli in assiale

ph_a_axial = np.array(data_axial['CrystalA_EDep'])

cond_a_axial = ph_a_axial !=0 #tolgo gli eventi in cui i cristalli non vengono colpiti, cioe quando in ph_i = 0 (puo succedere che la part passa i tracker e non attraversa i crsitalli )


ph_b_axial = np.array(data_axial['CrystaB_EDep'])

cond_b_axial = ph_b_axial !=0

ph_c_axial = np.array(data_axial['CrystalC_EDep'])

cond_c_axial = ph_c_axial !=0
 
ph_axial = {'crisA': ph_a_axial, 'crisB': ph_b_axial, 'crisC': ph_c_axial}

cond_axial = {'crisA': cond_a_axial, 'crisB': cond_b_axial, 'crisC': cond_c_axial}

fig, ax = plt.subplots(3,1)

ax = ax.flatten()

fig.set_size_inches(15,15)
    
for i,key in enumerate(ph_axial.keys()) :
    
    
    h, bins = np.histogram(ph_amo[key][cond_amo[key]], bins = 100, range = [0,4]) #istogrammo solo gli eventi che hanno depositato qualcosa nel cristallo, e non rigorosamente zero 

    binc = bins[:-1]+(bins[1]-bins[0])/2
    
    
    ax[i].plot(binc,h, ds='steps-mid',color = 'darkgreen', label = f'{i+1}-th crystal- axial config')

    ax[i].grid()
    
    ax[i].set_yscale('log')
    
    ax[i].legend(fontsize = 18)


